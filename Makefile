# -*- coding: utf-8 -*-
# :Project:    --
# :Created:   dom 4 ott 2020, 03:53:30
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#

.PHONY: run
run:
	python kitchensink.py

.PHONY: tests
tests:
	mypy *py
