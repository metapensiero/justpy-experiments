{ pkgs ? import <nixpkgs> {} }:
  with pkgs;
  let
    pythonPackages = python3Packages;
  in mkShell rec {
    name = "impurePythonEnv";
    venvDir = "./.venv";
    buildInputs = [
      git
      gnumake
      # A python interpreter including the 'venv' module is required to bootstrap
      # the environment.
      pythonPackages.python
      # This execute some shell code to initialize a venv in $venvDir before
      # dropping into the shell
      pythonPackages.venvShellHook

      pythonPackages.h11
      pythonPackages.mypy
      pythonPackages.pandas
      pythonPackages.starlette
      pythonPackages.uvloop
    ];

    postShellHook = ''
      pip install -r requirements.txt
    '';

}
