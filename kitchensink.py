# -*- coding: utf-8 -*-
# :Project:    --
# :Created:   sab 3 ott 2020, 17:59:36
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#
from __future__ import annotations

import typing
from typing import Union, Sequence, Callable, Optional, Any

import justpy as jp # type: ignore

import db
import clock
import collaborative_editor
import hello_world
import message_board

class KitchenPage(typing.NamedTuple):
    func: Union[Callable[[], jp.WebPage], Callable[[Any], jp.WebPage]]
    route: str
    link_txt: str
    pre_txt: Optional[str] = ''
    post_txt: Optional[str] = ''


def home(pages: typing.Sequence[KitchenPage]) -> jp.WebPage:
    head = '''
    <link href="//unpkg.com/cirrus-ui" rel="stylesheet">
    '''
    page = jp.WebPage(template_file='quasar.html', head_html=head)
    page.add(jp.H3(text="Some experiments with JustPy"))
    lst = jp.Ul()
    for kp in pages:
        lst.add(jp.Li() + (jp.P(text=kp.pre_txt) + jp.A(href=kp.route, text=kp.link_txt) +
                           jp.Span(text=kp.post_txt)))
    page.add(lst)
    return page


PAGES = [
    KitchenPage(hello_world.hello_world, '/hello', 'hello world', 'Try the '),
    KitchenPage(clock.clock, '/clock', 'clock', 'A simple wall '),
    KitchenPage(collaborative_editor.editor, '/editor', 'editor', 'Open a collaborative ',
     ' in multiple tabs'),
    KitchenPage(db.db_test, '/db', 'db grid', 'Open up a '),
    KitchenPage(message_board.board, '/messages', 'message board', 'Use a simple ')
]


if __name__ == '__main__':
    for kp in PAGES:
        jp.Route(kp.route, kp.func)
    jp.justpy(lambda : home(PAGES))
