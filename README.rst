.. -*- coding: utf-8 -*-
.. :Project:   JustPy tests --
.. :Created:   dom 4 ott 2020, 03:56:54
.. :Author:    Alberto Berti <alberto@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020 Alberto Berti
..

===================
 Some JustPy tests
===================

These are some tests with the JustPy__ framework. It's still a bit rough but
it's promising.

To run the tests you will have to:

1. install Nix__;
2. run `nix-shell` in the directory;
3. execute `make run`;


__ https://justpy.io
__ https://nixos.org/download.html
