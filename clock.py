# -*- coding: utf-8 -*-
# :Project:    --
# :Created:   sab 3 ott 2020, 13:42:42
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#

from __future__ import annotations

import asyncio
import time

import justpy as jp # type: ignore


def clock() -> jp.WebPage:
    wp = jp.WebPage(delete_flag=False)
    clock_div = jp.Span(text='Loading...',
                        classes='text-5xl m-1 p-1 bg-gray-300 font-mono', a=wp)
    wp.cdiv = clock_div
    wp.on('page_ready', clock_init)
    return wp

async def clock_counter(wp) -> None:
    while True:
        wp.cdiv.text = time.strftime("%a, %d %b %Y, %H:%M:%S", time.localtime())
        jp.run_task(wp.update())
        await asyncio.sleep(1)

async def clock_init(wp, data) -> None:
    jp.run_task(clock_counter(wp))


if __name__ == '__main__':
    jp.justpy(clock)
