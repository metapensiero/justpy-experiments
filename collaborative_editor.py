# -*- coding: utf-8 -*-
# :Project:    --
# :Created:   sab 3 ott 2020, 13:44:50
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#

from __future__ import annotations

import justpy as jp # type: ignore


def editor() -> jp.QuasarPage:
    wp = jp.QuasarPage(delete_flag=False)
    editor = jp.QEditor(a=wp, kitchen_sink=True, height='94vh')
    return wp


if __name__ == '__main__':
    jp.justpy(editor)
