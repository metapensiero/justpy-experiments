# -*- coding: utf-8 -*-
# :Project:    --
# :Created:   sab 3 ott 2020, 13:00:50
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#

import justpy as jp # type: ignore


def hello_world():
    wp = jp.WebPage()
    jp.Hello(a=wp)
    return wp


if __name__ == '__main__':
    jp.justpy(hello_world)
